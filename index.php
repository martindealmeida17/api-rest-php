<?php

// Definindo o tipo de resposta
header("Content-Type: application/json; charset=UTF-8");

// Verificando o método HTTP
if($_SERVER["REQUEST_METHOD"] != "GET") {
    http_response_code(405);
    echo json_encode(array("mensagem" => "Método não permitido."));
    exit();
}

// Configurações do banco de dados
$host = "localhost";
$dbname = "test";
$user = "root";
$password = "";

// Conectando ao banco de dados
try {
    $pdo = new PDO("mysql:host=$host;dbname=$dbname", $user, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    http_response_code(500);
    echo json_encode(array("mensagem" => "Erro ao conectar ao banco de dados."));
    exit();
}

// Buscando os dados no banco de dados
$stmt = $pdo->prepare("SELECT * FROM produtos");
$stmt->execute();
$produtos = $stmt->fetchAll(PDO::FETCH_ASSOC);

// Retornando os dados como JSON
echo json_encode($produtos);
exit();

?>
